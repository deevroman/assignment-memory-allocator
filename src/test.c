#ifndef TEST_H
#define TEST_H

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>
#include <stdlib.h>

#define DEFAULT_HEAP_SIZE 1000
struct block_header *memory_block = NULL;

static size_t get_number_of_blocks(struct block_header *block) {
    size_t count = 0;
    while (block->next) {
        count++;
        block = block->next;
    }
    return count;
}


void test_malloc() {
    printf("\nTest #1: Malloc test\n");
    size_t test_size = 100;
    void *malloc = _malloc(test_size);
    debug_heap(stdout, memory_block);
    size_t capacity = memory_block->capacity.bytes;
    _free(malloc);
    assert(capacity == test_size);
    printf("Success\n");
}


void test_free_one_block() {
    printf("\nTest #2: Free one block test\n");
    size_t test_size1 = 10;
    size_t test_size2 = 20;
    void *malloc1 = _malloc(test_size1);
    void *malloc2 = _malloc(test_size2);
    debug_heap(stdout, memory_block);
    size_t block_number1 = get_number_of_blocks(memory_block);
    _free(malloc2);
    debug_heap(stdout, memory_block);
    size_t block_number2 = get_number_of_blocks(memory_block);
    _free(malloc1);
    assert(block_number1 == 2 && block_number2 == 1);
    printf("Success\n");
}

void test_free_two_blocks() {
    printf("\nTest #3: Free two blocks test\n");
    size_t test_size1 = 10;
    size_t test_size2 = 10;
    size_t test_size3 = 20;
    void *malloc1 = _malloc(test_size1);
    void *malloc2 = _malloc(test_size2);
    void *malloc3 = _malloc(test_size3);
    debug_heap(stdout, memory_block);
    _free(malloc3);
    _free(malloc2);
    size_t block_number = get_number_of_blocks(memory_block);
    debug_heap(stdout, memory_block);
    _free(malloc1);

    assert (block_number == 1);
    printf("Success\n");
}

void test_grow_heap() {
    printf("\nTest #4: Grow heap test\n");
    const size_t test_size1 = DEFAULT_HEAP_SIZE - 200;
    const size_t test_size2 = 200;
    void *malloc1 = _malloc(test_size1);
    void *malloc2 = _malloc(test_size2);
    debug_heap(stdout, memory_block);
    size_t block_number = get_number_of_blocks(memory_block);
    _free(malloc1);
    _free(malloc2);

    assert(block_number == 2);
    printf("Success\n");

}

void test_grow_heap_new_region_alloc() {
    printf("\nTest #5: Grow heap test with allocating new region\n");
    debug_heap(stdout, memory_block);
    const size_t test_size1 = DEFAULT_HEAP_SIZE;
    const size_t test_size2 = 100;
    void *malloc1 = _malloc(test_size1);
    debug_heap(stdout, memory_block);
    map_pages(memory_block, 900, MAP_SHARED);
    debug_heap(stdout, memory_block);
    void *malloc2 = _malloc(test_size2);
    debug_heap(stdout, memory_block);
    _free(malloc1);
    _free(malloc2);

    printf("Success\n");
}

void test() {
    printf("Starting tests\n");
    memory_block = (struct block_header *) heap_init(DEFAULT_HEAP_SIZE);
    if (memory_block == NULL) {
        printf("Heap init failed\n");
        exit(1);
    }
    test_malloc();
    test_free_one_block();
    test_free_two_blocks();
    test_grow_heap();
    test_grow_heap_new_region_alloc();
    printf("All tests are passed!\n");
}

#endif
